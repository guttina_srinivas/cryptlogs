# Encrypted Logs for Python

Title says it all.

---

## How???
~~~~
import logging
from CrypticLogs.crypt_logger import EncryptedStreamHandler
eh = EncryptedStreamHandler(pubkstr) # EC-SECP521R1 key in PEM
logger = logging.getLogger()
for h in logger.handlers:   # Optional
    logger.removeHandler(h)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
eh.setFormatter(formatter)
logger.addHandler(eh)

logger.debug('debug message')
logger.info('info message')
logger.warn('warn message')
logger.error('error message')
logger.critical('critical message')
~~~~

---

## Decrypt the logs
~~~~
from decrypt_logs import EncLogDecryptor
ed = EncLogDecryptor('cmn_pvtk.file')  # PEM encoded file
ed.decrypt()
~~~~

---

## TODO

0. Document
1. Create an optimized version like Cython or so.
2. Maybe, move to compiled version written in C++.
3. Binary downloads.
