import os
import sys
import struct
from logging.handlers import MemoryHandler
import encrutils


class EncryptedStreamHandler(MemoryHandler):

    # Try and keep buffer within one page. Any overheads should
    # fit within the remaining 2KB.
    BUFLEN = 2048

    def __init__(self, pubkey, logspath='.', logname='logfile'):
        MemoryHandler.__init__(self, 4096)
        self.logspath = logspath
        self.logname = logname
        self.pubkey = pubkey
        self.ofd = None
        self.shkey = None
        self.open_file()
        self.debug = True
        self.buffer = bytearray([0 for ii in range(self.BUFLEN)])
        self.bufidx = 0
        self.wbuflen = 0
        self.wbuf = None

    def swap_buf(self):
        # TODO Implement locking for thread-safe swaps
        self.wbuf = self.buffer
        self.buffer = bytearray([0 for ii in range(self.BUFLEN)])
        self.wbuflen = self.bufidx
        self.bufidx = 0

    def open_file(self):
        pubkeystr, self.shkey = encrutils.gen_key(self.pubkey)
        if self.ofd is not None:
            try:
                self.ofd.close()
                self.ofd = None
            except:
                pass
        logname = self.logname + '_{}.log'.format(self.get_file_id())
        self.ofd = open(self.logspath + os.path.sep + logname, 'wb+')
        self.ofd.write('VENCLGFMT'.encode())
        keylen = struct.pack("<I", len(pubkeystr))
        self.ofd.write(keylen)
        self.ofd.write(pubkeystr)

    def get_file_id(self):
        # TODO look up logs path and return next log file id
        return 1

    def shouldFlush(self, record):
        # AES-128 operates on blocks of 128 bits each!!! To be safer and lazyier (yeah, live with
        # the spelling), we use 128 bytes as block length.
        if (self.bufidx + len(self.format(record).encode())) >= self.BUFLEN:
            return True
        return False

    def save_buf(self, buff):
        self.ofd.write(buff)
        self.print_buf(buff)

    def print_buf(self, buff):
        if self.debug is False:
            return
        for b in buff:
            self.conswrite('{0:02X} '.format(b))

    def emit(self, record):
        if self.shouldFlush(record):
            self.flush()
        recstr = self.format(record)
        self.conswrite(recstr)
        recbuf = recstr.encode()
        for b in recbuf:
            self.buffer[self.bufidx] = b
            self.bufidx += 1

    def flush(self):
        self.swap_buf()
        offset = 0
        blklen = 16
        buflen = self.wbuflen + (16 - (self.wbuflen % 16))
        while buflen > 16:
            # self.print_buf(self.buffer[offset:blklen])
            encbuf = encrutils.enc_dec_buf(self.shkey, self.wbuf[offset:blklen])
            self.save_buf(bytearray(encbuf))
            offset += 16
            blklen = offset + 16
            buflen -= 16

    def conswrite(self, buf):
        if self.debug is False:
            return
        sys.stdout.write(buf)
        sys.stdout.flush()