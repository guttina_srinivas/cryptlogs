import struct
import encrutils


class EncLogDecryptor:
    BLOCK_SIZE = 16

    def __init__(self, pvtkfname, fname='logfile_1.log', ofname='decrypted1.log'):
        self.logfname = fname
        self.logfd = open(fname, 'rb')
        self.outfd = open(ofname, 'wb+')
        self.pvtkstr = self.load_pvtk(pvtkfname)
        self.pubkstr = self.load_pubk()
        self.key = encrutils.compute_key(self.pvtkstr, self.pubkstr)

    def decrypt_block(self, buf):
        return encrutils.enc_dec_buf(self.key, buf)

    def load_pubk(self):
        logfd = self.logfd
        signature = logfd.read(9)
        if signature != 'VENCLGFMT'.encode():
            raise TypeError('{}: Signature mismatch. Cannot continue'.format(self.logfname))

        kstrlen = struct.unpack("<I", logfd.read(4))
        pubkstr = logfd.read(kstrlen[0])

        return pubkstr

    def load_pvtk(self, pvtkfname):
        with open(pvtkfname) as pkfd:
            pvtkstr = ''.join(pkfd.readlines()).encode()
            return pvtkstr

    def decrypt(self):
        logfd = self.logfd
        while True:
            block = logfd.read(self.BLOCK_SIZE)
            if block is None:
                return
            if len(block) < self.BLOCK_SIZE:
                return
            decbuf = self.decrypt_block(block)
            self.outfd.write(bytearray(decbuf))

# End of file
