from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization


def enc_dec_buf(key, data):
    datalen = len(data)
    if (datalen < 16) or len(key) < 16:
        raise TypeError('{}: Buffer (len {}) must be at least 16 bytes long'.format(__name__, len(data)))
    retbuf = [0 for ii in range(16)]
    # TODO Implement AES128 here!!!
    for ii in range(16):
        retbuf[ii] = data[ii] ^ key[ii]
    return bytearray(retbuf)


def compute_key(pvtkstr, pubkstr):
    pubk = serialization.load_pem_public_key(pubkstr, backend=default_backend())
    pvtk = serialization.load_pem_private_key(pvtkstr, None, backend=default_backend())
    shkey = pvtk.exchange(ec.ECDH(), pubk)
    return shkey


def gen_key(pubkeystr):
    pubkey = serialization.load_pem_public_key(pubkeystr,
                                               backend=default_backend())
    lcl_pvtk = ec.generate_private_key(ec.SECP521R1, default_backend())
    lcl_pubk = lcl_pvtk.public_key()
    lcl_pubk_str = lcl_pubk.public_bytes(encoding=serialization.Encoding.PEM,
                                         format=serialization.PublicFormat.SubjectPublicKeyInfo)
    shkey = lcl_pvtk.exchange(ec.ECDH(), pubkey)
    return lcl_pubk_str, shkey


